package me.elddenmedio.SanPay1.models;

import java.sql.Date;
import java.util.List;

import org.json.JSONObject;

public class CardModel {
	private JSONObject userInfo;
	private String aliasTarjeta;
	private String titularTarjeta;
	private String numeroTarjeta;
	private String vencimiento;
	private String cvv;
	private Integer error;

	public JSONObject getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(JSONObject userInfo) {
		this.userInfo = userInfo;
	}

	public String getAliasTarjeta() {
		return aliasTarjeta;
	}

	public void setAliasTarjeta(String aliasTarjeta) {
		this.aliasTarjeta = aliasTarjeta;
	}

	public String getTitularTarjeta() {
		return titularTarjeta;
	}

	public void setTitularTarjeta(String titularTarjeta) {
		this.titularTarjeta = titularTarjeta;
	}

	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}

	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}

	public String getVencimiento() {
		return vencimiento;
	}

	public void setVencimiento(String vencimiento) {
		this.vencimiento = vencimiento;
	}

	public String getCvv() {
		return cvv;
	}

	public void setCvv(String cvv) {
		this.cvv = cvv;
	}

	public Integer getError() {
		return error;
	}

	public void setError(Integer error) {
		this.error = error;
	}

	@Override
	public String toString() {
		return "CardModel [userInfo=" + userInfo + ", aliasTarjeta=" + aliasTarjeta + ", titularTarjeta="
				+ titularTarjeta + ", numeroTarjeta=" + numeroTarjeta + ", vencimiento=" + vencimiento + ", cvv=" + cvv
				+ ", error=" + error + "]";
	}
}
