package me.elddenmedio.SanPay1.interfaces;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import me.elddenmedio.SanPay1.entities.CardsEntity;

@Repository
//public interface CardsInterface extends CrudRepository<CardsEntity, Long> {
public interface CardsInterface extends JpaRepository<CardsEntity, Long> {
	List<CardsEntity> findByCustomerId(Long customerID);
}
