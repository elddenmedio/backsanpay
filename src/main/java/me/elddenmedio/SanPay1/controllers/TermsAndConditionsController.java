package me.elddenmedio.SanPay1.controllers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import me.elddenmedio.SanPay1.entities.CustomerEntity;
import me.elddenmedio.SanPay1.entities.CustomerTyTEntity;
import me.elddenmedio.SanPay1.entities.RequestLogEntity;
import me.elddenmedio.SanPay1.entities.TermsAndConditionsEntity;
import me.elddenmedio.SanPay1.interfaces.CustomerTyTInterface;
import me.elddenmedio.SanPay1.interfaces.RequestLogInterface;
import me.elddenmedio.SanPay1.interfaces.TermsAndConditionsInterface;
import me.elddenmedio.SanPay1.utils.RandomStringUtils;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/")
public class TermsAndConditionsController {

	@Autowired
	TermsAndConditionsInterface termsAndConditionsInterfaceLocal;

	@Autowired
	RequestLogInterface requestLogInterfaceLocal;
	
	@Autowired
	CustomerTyTInterface customerTyTInterfaceLocal;

	public TermsAndConditionsController(TermsAndConditionsInterface termsAndConditionsInterfaceLocal) {
		this.termsAndConditionsInterfaceLocal = termsAndConditionsInterfaceLocal;
	}

	@GetMapping("/terms-and-conditions")
	public ResponseEntity<Object> getTerms(@RequestParam(required = false) String request) { // @RequestHeader("Authorization") String authorization, @RequestHeader("x-geolocation") String geolocation, 
		try {
			//RequestLogEntity _log = new RequestLogEntity("/terms-and-conditions", "GET", authorization, geolocation,
			//		null);
			//requestLogInterfaceLocal.save(_log);

			JSONObject _return = new JSONObject();
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			
			CustomerTyTEntity _customerTYT = customerTyTInterfaceLocal.getById(1l);

			TermsAndConditionsEntity _terms = termsAndConditionsInterfaceLocal.getById(1L);
			RandomStringUtils _randomAlphaNum = new RandomStringUtils(18);

			JSONObject _termsJSON = new JSONObject();
			JSONObject _termsInfo = new JSONObject();

			_termsInfo.put("key", "4e20fbb243684d9eb19ff33a50ee422e");
			_termsInfo.put("name", "Terminos y Condiciones");
			_termsInfo.put("format", "html");
			_termsInfo.put("version", "V1.0");
			_termsInfo.put("file", _terms.getTerms());
			_termsInfo.put("type", "text/html");
			//_termsInfo.put("text", _terms.getTerms());

			_termsJSON.put("key", "4e20fbb243684d9eb19ff33a50ee422e");
			_termsJSON.put("accepted_terms", _customerTYT.getAccept());
			_termsJSON.put("operation_date", dateFormat.format(today()));
			_termsJSON.put("document", _termsInfo);

			_return.put("data", _termsJSON);
			_return.put("result", "success");
			_return.put("notifications", JSONObject.NULL);

			return new ResponseEntity<>(_return.toString(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	private Date today() {
		final Calendar cal = Calendar.getInstance();
		return cal.getTime();
	}
}
