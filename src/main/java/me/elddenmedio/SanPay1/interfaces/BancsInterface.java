package me.elddenmedio.SanPay1.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import me.elddenmedio.SanPay1.entities.BancsEntity;;

@Repository
//public interface BancsInterface extends CrudRepository<BancsEntity, Long>{
public interface BancsInterface extends JpaRepository<BancsEntity, Long> {

}
