package me.elddenmedio.SanPay1.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.sun.istack.NotNull;

@Entity
public class BancsEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "name")
	@NotNull
	private String name;

	@Column(name = "bank_institution_key")
	@NotNull
	private Integer bank_institution_key;

	@OneToMany(mappedBy = "banc", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	private List<CardsEntity> customerCards = new ArrayList<>();

	public BancsEntity() {
	}

	public BancsEntity(String name, Integer bank_institution_key, List<CardsEntity> customerCards) {
		this.name = name;
		this.bank_institution_key = bank_institution_key;
		this.customerCards = customerCards;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getBank_institution_key() {
		return bank_institution_key;
	}

	public void setBank_institution_key(Integer bank_institution_key) {
		this.bank_institution_key = bank_institution_key;
	}

	public List<CardsEntity> getCustomerCards() {
		return customerCards;
	}

	public void setCustomerCards(List<CardsEntity> customerCards) {
		this.customerCards = customerCards;
	}

	@Override
	public String toString() {
		return "BancsEntity [id=" + id + ", name=" + name + ", bank_institution_key=" + bank_institution_key
				+ ", customerCards=" + customerCards + "]";
	}
}
