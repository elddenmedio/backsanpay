package me.elddenmedio.SanPay1.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import me.elddenmedio.SanPay1.entities.PaymentEntity;

@Repository
//public interface CardsInterface extends CrudRepository<PaymentEntity, Long> {
public interface PaymentInterface extends JpaRepository<PaymentEntity, Long> {

}
