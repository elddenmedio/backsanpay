package me.elddenmedio.SanPay1.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.sun.istack.NotNull;

@Entity
@Table(name = "termsAndConditions")
public class CustomerTyTEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "accept_terms")
	@NotNull
	private Boolean accept;
	
	@Column(name = "customer_id")
	@NotNull
	private Long customer_id;

	public CustomerTyTEntity() {
	}

	public CustomerTyTEntity(Boolean accept, Long customer_id) {
		this.accept = accept;
		this.customer_id = customer_id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getAccept() {
		return accept;
	}

	public void setAccept(Boolean accept) {
		this.accept = accept;
	}

	public Long getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(Long customer_id) {
		this.customer_id = customer_id;
	}

	@Override
	public String toString() {
		return "CustomerTyTEntity [id=" + id + ", accept=" + accept + ", customer_id=" + customer_id + "]";
	}
}
