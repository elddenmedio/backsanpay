package me.elddenmedio.SanPay1.controllers;

import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import me.elddenmedio.SanPay1.entities.CardsEntity;
import me.elddenmedio.SanPay1.entities.CustomerEntity;
import me.elddenmedio.SanPay1.entities.PaymentEntity;
import me.elddenmedio.SanPay1.entities.RequestLogEntity;
import me.elddenmedio.SanPay1.entities.ServiceEntity;
import me.elddenmedio.SanPay1.interfaces.CardsInterface;
import me.elddenmedio.SanPay1.interfaces.CustomerInterface;
import me.elddenmedio.SanPay1.interfaces.PaymentInterface;
import me.elddenmedio.SanPay1.interfaces.RequestLogInterface;
import me.elddenmedio.SanPay1.interfaces.ServicesInterface;
import me.elddenmedio.SanPay1.models.PaymentsModel;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/payments")
public class PaymentController {

	@Autowired
	PaymentInterface paymentInterfaceLocal;

	@Autowired
	CustomerInterface customerInterfaceLocal;

	@Autowired
	ServicesInterface servicesInterfaceLocal;

	@Autowired
	CardsInterface cardsInterfaceLocal;

	@Autowired
	RequestLogInterface requestLogInterfaceLocal;

	ServiceEntity _serviceTMP;
	CardsEntity _cardTMP;

	public PaymentController(PaymentInterface paymentInterfaceLocal) {
		super();
		this.paymentInterfaceLocal = paymentInterfaceLocal;
	}

	@PostMapping("/direct-debits")
	public ResponseEntity<Object> postTyT(@RequestHeader("Authorization") String authorization,
			@RequestHeader("x-geolocation") String geolocation, @RequestBody PaymentsModel payments) {
		try {
			RequestLogEntity _log = new RequestLogEntity("/payments/direct-debits", "POST", authorization, geolocation,
					payments.toString());
			requestLogInterfaceLocal.save(_log);

			JSONObject _return = new JSONObject();

			CustomerEntity _customer = customerInterfaceLocal.getById(1L);
//			String _tmpName = _customer.getName();
			List<ServiceEntity> _service = servicesInterfaceLocal.findAll();
			_service.forEach(service -> {
				if (service.getKey() == Integer.parseInt(payments.getOperation().getKey())) {
					_serviceTMP = service;
				}
			});
			List<CardsEntity> _card = cardsInterfaceLocal.findAll();
			_card.forEach(card -> {
				if (card.getBanc().getBank_institution_key() == Integer
						.parseInt(payments.getOperation().getPayer().getBank_institution_key())) {
					_cardTMP = card;
				}
			});

			PaymentEntity _payment = new PaymentEntity(_customer, _serviceTMP, _cardTMP);
			paymentInterfaceLocal.save(_payment);

			JSONObject _data = new JSONObject();
			_data.put("message", "The operation for the message CoDi was successfully saved.");

			_return.put("data", _data);
			_return.put("notifications", JSONObject.NULL);

			return new ResponseEntity<>(_return.toString(), HttpStatus.CREATED);
		} catch (Exception e) {
			JSONObject _return = new JSONObject();
			_return.put("data", JSONObject.NULL);
			_return.put("notifications", "Some data are incorrect");

			return new ResponseEntity<>(_return.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
