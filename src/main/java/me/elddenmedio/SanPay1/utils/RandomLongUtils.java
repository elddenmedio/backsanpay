package me.elddenmedio.SanPay1.utils;

public class RandomLongUtils {

	public final Long randomNumeric;

	/**
	 * Generate a random string.
	 */
	public Long nextLong() {
		return this.randomNumeric;
	}

	/**
	 * Create a numeric generator.
	 */
	public RandomLongUtils(Integer min, Integer max) {
		this.randomNumeric = (long) (min + (int) (Math.random() * max));
	}

	public RandomLongUtils(Integer lengthVal) {
		this.randomNumeric = new Long(lengthVal);
	}
}
