package me.elddenmedio.SanPay1.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.sun.istack.NotNull;

@Entity
@Table(name = "service")
public class ServiceEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "key")
	@NotNull
	private Integer key;

	@Column(name = "name")
	@NotNull
	private String name;

	@Column(name = "shortName")
	@NotNull
	private String shortName;

	@Column(name = "abbreviatedName")
	@NotNull
	private String abbreviatedName;

	@Column(name = "serviceCategory")
	@NotNull
	private String serviceCategory;

	@Column(name = "mobilReferer")
	@NotNull
	private String mobilReferer;

	@Column(name = "agreement")
	@NotNull
	private Integer agreement;

	@Column(name = "color")
	@NotNull
	private String color;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "category_id", nullable = false)
	private CategoryServiceEntity category;

	@OneToMany(mappedBy = "service", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	private List<PaymentEntity> payments = new ArrayList<>();

	public ServiceEntity() {
	}

	public ServiceEntity(Integer key, String name, String shortName, String abbreviatedName, String serviceCategory,
			String mobilReferer, Integer agreement, String color, CategoryServiceEntity category,
			List<PaymentEntity> customerPayments) {
		this.key = key;
		this.name = name;
		this.shortName = shortName;
		this.abbreviatedName = abbreviatedName;
		this.serviceCategory = serviceCategory;
		this.mobilReferer = mobilReferer;
		this.agreement = agreement;
		this.color = color;
		this.category = category;
		this.payments = customerPayments;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getAbbreviatedName() {
		return abbreviatedName;
	}

	public void setAbbreviatedName(String abbreviatedName) {
		this.abbreviatedName = abbreviatedName;
	}

	public String getServiceCategory() {
		return serviceCategory;
	}

	public void setServiceCategory(String serviceCategory) {
		this.serviceCategory = serviceCategory;
	}

	public String getMobilReferer() {
		return mobilReferer;
	}

	public void setMobilReferer(String mobilReferer) {
		this.mobilReferer = mobilReferer;
	}

	public Integer getAgreement() {
		return agreement;
	}

	public void setAgreement(Integer agreement) {
		this.agreement = agreement;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public CategoryServiceEntity getCategory() {
		return category;
	}

	public void setCategory(CategoryServiceEntity category) {
		this.category = category;
	}

	public List<PaymentEntity> getCustomerPayments() {
		return payments;
	}

	public void setCustomerPayments(List<PaymentEntity> customerPayments) {
		this.payments = customerPayments;
	}

	@Override
	public String toString() {
		return "ServiceEntity [id=" + id + ", key=" + key + ", name=" + name + ", shortName=" + shortName
				+ ", abbreviatedName=" + abbreviatedName + ", serviceCategory=" + serviceCategory + ", mobilReferer="
				+ mobilReferer + ", agreement=" + agreement + ", color=" + color + ", category=" + category
				+ ", customerPayments=" + payments + "]";
	}
}
