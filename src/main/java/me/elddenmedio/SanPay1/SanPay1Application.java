package me.elddenmedio.SanPay1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SanPay1Application {

	public static void main(String[] args) {
		SpringApplication.run(SanPay1Application.class, args);
	}

}
