package me.elddenmedio.SanPay1.models;

public class BasicCustomerInfoModel {

	private Integer buc;
	private String personalIdentifier;

	public Integer getBuc() {
		return buc;
	}

	public void setBuc(Integer buc) {
		this.buc = buc;
	}

	public String getPersonalIdentifier() {
		return personalIdentifier;
	}

	public void setPersonalIdentifier(String personalIdentifier) {
		this.personalIdentifier = personalIdentifier;
	}

	@Override
	public String toString() {
		return "BasicCustomerInfo [buc=" + buc + ", personalIdentifier=" + personalIdentifier + "]";
	}
}
