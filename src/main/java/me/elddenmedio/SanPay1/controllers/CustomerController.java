package me.elddenmedio.SanPay1.controllers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import me.elddenmedio.SanPay1.entities.CustomerEntity;
import me.elddenmedio.SanPay1.entities.CustomerTyTEntity;
import me.elddenmedio.SanPay1.entities.RequestLogEntity;
import me.elddenmedio.SanPay1.interfaces.CustomerInterface;
import me.elddenmedio.SanPay1.interfaces.CustomerTyTInterface;
import me.elddenmedio.SanPay1.interfaces.RequestLogInterface;
import me.elddenmedio.SanPay1.models.TermsAndConditionsModel;
import me.elddenmedio.SanPay1.utils.RandomStringUtils;

// @Controller
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/customers")
public class CustomerController {

	@Autowired
	CustomerInterface customerInterfaceLocal;

	@Autowired
	CustomerTyTInterface customerTyTInterfaceLocal;

	@Autowired
	RequestLogInterface requestLogInterfaceLocal;

	// private final CustomerInterface customerInterfaceLocal;

	public CustomerController(CustomerInterface customerInterfaceLocal) {
		this.customerInterfaceLocal = customerInterfaceLocal;
	}

	@GetMapping("/me")
	public ResponseEntity<Object> getCustomer(@RequestParam(required = false) String request) { // @RequestHeader("Authorization") String authorization, @RequestHeader("x-geolocation") String geolocation, 
		try {
			//RequestLogEntity _log = new RequestLogEntity("/customers/me", "GET", authorization, geolocation, null);
			//requestLogInterfaceLocal.save(_log);

			JSONObject _return = new JSONObject();

			Optional<CustomerEntity> _customer = customerInterfaceLocal.findById((long) 1);
			CustomerEntity _customer2 = customerInterfaceLocal.getById(1L);
			String nameC = _customer2.getName();

			if (_customer.isPresent()) {
				JSONObject _contactInfo = new JSONObject();
				_contactInfo.put("type", _customer.get().getType());
				_contactInfo.put("phone_number", _customer.get().getContact_info_phone_number());
				_contactInfo.put("mobile_number", _customer.get().getContact_info_mobile_number());
				_contactInfo.put("email", _customer.get().getContact_info_email());
				_contactInfo.put("status", _customer.get().getContact_info_phone_status());

				JSONObject _bankSegment = new JSONObject();
				_bankSegment.put("key", _customer.get().getBank_segment_key());
				_bankSegment.put("name", _customer.get().getBank_segment_name());

				JSONObject _data = new JSONObject();

				_data.put("key", _customer.get().getKey());
				_data.put("buc", _customer.get().getBuc());
				_data.put("name", _customer.get().getName());
				_data.put("second_name", _customer.get().getSecond_name());
				_data.put("last_name", _customer.get().getLast_name());
				_data.put("type", _customer.get().getType());
				_data.put("status", (_customer.get().getStatus()) ? "ACTIVE" : "");
				_data.put("personal_identifier", _customer.get().getPersonalIdentifier());
				_data.put("bank_segment", _bankSegment);
				_data.put("contact_method", _customer.get().getContact_method());
				_data.put("last_connection", _customer.get().getLast_connection());
				_data.put("contact_info", _contactInfo);

				_return.put("data", _data);
				_return.put("notifications", JSONObject.NULL);

				return new ResponseEntity<>(_return.toString(), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/me/settings/terms")
	public ResponseEntity<Object> getTyT(@RequestParam(required = false) String error) { // @RequestHeader("Authorization") String authorization, @RequestHeader("x-geolocation") String geolocation, 
		try {
			//RequestLogEntity _log = new RequestLogEntity("/customers/me/settings/terms", "GET", authorization,
			//		geolocation, null);
			//requestLogInterfaceLocal.save(_log);

			JSONObject _return = new JSONObject();
			JSONObject _data = new JSONObject();
			JSONObject _notification = new JSONObject();
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

			if (error == null) {
				CustomerTyTEntity _customerTyT = customerTyTInterfaceLocal.getById(1L);
				// System.out.println("customerTyT: " + _customerTyT);
				// System.out.println("TyT: " + _customerTyT.getAccept());
				if (_customerTyT.getAccept()) {
					RandomStringUtils _randomAlphaNum = new RandomStringUtils(32);

					_data.put("key", _randomAlphaNum.toString().split("@")[1]);
					_data.put("accepted_terms", true);
					_data.put("operation_date", dateFormat.format(today()));
					_data.put("customer", _customerTyT.getAccept());

					_return.put("data", _data);
					_return.put("notifications", JSONObject.NULL);

					return new ResponseEntity<>(_return.toString(), HttpStatus.OK);
				} else {
					RandomStringUtils _randomAlphaNum = new RandomStringUtils(18);

					_notification.put("level", "WARNING");
					_notification.put("code", "7397b7a111"); // _randomAlphaNum.toString().split("@")[1]);
					_notification.put("message", "TEMPLATE__Error__TryLate");
					_notification.put("timestamp", dateFormat.format(today()));

					// _return.put("data", JSONObject.NULL);
					// _return.put("notifications", _notification);

				}
			} else {
				RandomStringUtils _randomAlphaNum = new RandomStringUtils(18);

				_notification.put("level", "ERROR");
				_notification.put("code", "7397b7a111"); // _randomAlphaNum.toString().split("@")[1]);
				_notification.put("message",
						"El código ingresado es incorrecto, favor de validarlo e ingresarlo nuevamente..");
				_notification.put("timestamp", dateFormat.format(today()));
			}

			_return.put("data", JSONObject.NULL);
			_return.put("notifications", _notification);

			return new ResponseEntity<>(_return.toString(), HttpStatus.UNPROCESSABLE_ENTITY);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("/me/settings/terms")
	public ResponseEntity<Object> postTyT(@RequestBody TermsAndConditionsModel terms) { // @RequestHeader("Authorization") String authorization, @RequestHeader("x-geolocation") String geolocation, 
		try {
			//RequestLogEntity _log = new RequestLogEntity("/customers/me/settings/terms", "POST", authorization,
			//		geolocation, terms.toString());
			//requestLogInterfaceLocal.save(_log);

			JSONObject _return = new JSONObject();

			if (terms.getError() == null) {
				CustomerTyTEntity _terms = customerTyTInterfaceLocal.getById(1L);
				_terms.setAccept(true);
				customerTyTInterfaceLocal.save(_terms);

				_return.put("data", "The agree of the terms and conditions was successful.");
				_return.put("notifications", false);

				return new ResponseEntity<>(_return.toString(), HttpStatus.CREATED);
			} else {
				JSONObject _notification = new JSONObject();
				_notification.put("level", "WARNING");
				_notification.put("code", "7397b7a111");
				_notification.put("message", "No updimos atender tu solicitud por ahora. Inténtalo después.");
				_notification.put("timestamp", terms.getOperation_date());

				_return.put("data", JSONObject.NULL);
				_return.put("notifications", _notification);

				return new ResponseEntity<>(_return.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	private Date today() {
		final Calendar cal = Calendar.getInstance();
		return cal.getTime();
	}
}
