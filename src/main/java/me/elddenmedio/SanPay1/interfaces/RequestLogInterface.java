package me.elddenmedio.SanPay1.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;

import me.elddenmedio.SanPay1.entities.RequestLogEntity;

public interface RequestLogInterface extends JpaRepository<RequestLogEntity, Long> {

}
