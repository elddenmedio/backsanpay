package me.elddenmedio.SanPay1.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import me.elddenmedio.SanPay1.entities.CategoryServiceEntity;

@Repository
public interface CategoryServiceInterface extends JpaRepository<CategoryServiceEntity, Long> {

}
