package me.elddenmedio.SanPay1.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {
	
	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

	public DateUtils() {
		final Calendar cal = Calendar.getInstance();
		dateFormat.format(cal.getTime());
	}

	public DateUtils(int pastDay) {
		final Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -pastDay);
		dateFormat.format(cal.getTime());
	}
}
