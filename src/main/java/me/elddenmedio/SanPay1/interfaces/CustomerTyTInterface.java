package me.elddenmedio.SanPay1.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import me.elddenmedio.SanPay1.entities.CustomerTyTEntity;

@Repository
//public interface CustomerTyTInterface extends CrudRepository<CustomerTyTEntity, Long> {
public interface CustomerTyTInterface extends JpaRepository<CustomerTyTEntity, Long> {

}
