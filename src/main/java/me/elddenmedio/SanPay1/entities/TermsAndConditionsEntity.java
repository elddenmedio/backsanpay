package me.elddenmedio.SanPay1.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.sun.istack.NotNull;

@Entity
@Table(name = "terms")

public class TermsAndConditionsEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "terms", length = 16777215)
	@NotNull
	private String terms;

	public TermsAndConditionsEntity() {
	}

	public TermsAndConditionsEntity(String terms) {
		this.terms = terms;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTerms() {
		return terms;
	}

	public void setTerms(String terms) {
		this.terms = terms;
	}

	@Override
	public String toString() {
		return "TermsAndConditionsEntity [id=" + id + ", terms=" + terms + "]";
	}
}
