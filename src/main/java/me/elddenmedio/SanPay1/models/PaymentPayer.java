package me.elddenmedio.SanPay1.models;

public class PaymentPayer {

	private String name;
	private String account_type;
	private String account_number;
	private String bank_institution_key;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAccount_type() {
		return account_type;
	}

	public void setAccount_type(String account_type) {
		this.account_type = account_type;
	}

	public String getAccount_number() {
		return account_number;
	}

	public void setAccount_number(String account_number) {
		this.account_number = account_number;
	}

	public String getBank_institution_key() {
		return bank_institution_key;
	}

	public void setBank_institution_key(String bank_institution_key) {
		this.bank_institution_key = bank_institution_key;
	}

	@Override
	public String toString() {
		return "PaymentPayer [name=" + name + ", account_type=" + account_type + ", account_number=" + account_number
				+ ", bank_institution_key=" + bank_institution_key + "]";
	}
}
