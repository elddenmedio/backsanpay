package me.elddenmedio.SanPay1.models;

import java.sql.Date;

public class TermsAndConditionsModel {

	private String key;
	private String device_fingerprint;
	private Date operation_date;
	private String document_version;
	private Boolean accept_terms;
	private Integer error;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getDevice_fingerprint() {
		return device_fingerprint;
	}

	public void setDevice_fingerprint(String device_fingerprint) {
		this.device_fingerprint = device_fingerprint;
	}

	public Date getOperation_date() {
		return operation_date;
	}

	public void setOperation_date(Date operation_date) {
		this.operation_date = operation_date;
	}

	public String getDocument_version() {
		return document_version;
	}

	public void setDocument_version(String document_version) {
		this.document_version = document_version;
	}

	public Boolean getAccept_terms() {
		return accept_terms;
	}

	public void setAccept_terms(Boolean accept_terms) {
		this.accept_terms = accept_terms;
	}

	public Integer getError() {
		return error;
	}

	public void setError(Integer error) {
		this.error = error;
	}

	@Override
	public String toString() {
		return "TermsAndConditionsModel [key=" + key + ", device_fingerprint=" + device_fingerprint
				+ ", operation_date=" + operation_date + ", document_version=" + document_version + ", accept_terms="
				+ accept_terms + ", error=" + error + "]";
	}
}
