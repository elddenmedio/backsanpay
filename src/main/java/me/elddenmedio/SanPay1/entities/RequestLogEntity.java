package me.elddenmedio.SanPay1.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.sun.istack.NotNull;
import com.sun.istack.Nullable;

@Entity
@Table(name = "requestlog")
public class RequestLogEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "url")
	@NotNull
	private String url;

	@Column(name = "verbose")
	@NotNull
	private String verbose;

	@Column(name = "authorization")
	@NotNull
	private String authorization;

	@Column(name = "geolocation")
	@Nullable
	private String geolocation;

	@Column(name = "body", length = 16777215)
	@Nullable
	private String body;

	public RequestLogEntity(String url, String verbose, String authorization, String geolocation, String body) {
		this.url = url;
		this.verbose = verbose;
		this.authorization = authorization;
		this.geolocation = geolocation;
		this.body = body;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getVerbose() {
		return verbose;
	}

	public void setVerbose(String verbose) {
		this.verbose = verbose;
	}

	public String getAuthorization() {
		return authorization;
	}

	public void setAuthorization(String authorization) {
		this.authorization = authorization;
	}

	public String getGeolocation() {
		return geolocation;
	}

	public void setGeolocation(String geolocation) {
		this.geolocation = geolocation;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	@Override
	public String toString() {
		return "RequestLogEntity [id=" + id + ", url=" + url + ", verbose=" + verbose + ", authorization="
				+ authorization + ", geolocation=" + geolocation + ", body=" + body + "]";
	}
}
