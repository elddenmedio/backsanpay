package me.elddenmedio.SanPay1.models;

public class PaymentOperationAmountModel {

	private String currency_code;
	private String amount;

	public String getCurrency_code() {
		return currency_code;
	}

	public void setCurrency_code(String currency_code) {
		this.currency_code = currency_code;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "PaymentOperationAmountModel [currency_code=" + currency_code + ", amount=" + amount + "]";
	}

}
