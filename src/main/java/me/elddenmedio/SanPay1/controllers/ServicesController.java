package me.elddenmedio.SanPay1.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import me.elddenmedio.SanPay1.entities.CustomerEntity;
import me.elddenmedio.SanPay1.entities.RequestLogEntity;
import me.elddenmedio.SanPay1.entities.ServiceEntity;
import me.elddenmedio.SanPay1.interfaces.CustomerInterface;
import me.elddenmedio.SanPay1.interfaces.CustomerTyTInterface;
import me.elddenmedio.SanPay1.interfaces.RequestLogInterface;
import me.elddenmedio.SanPay1.interfaces.ServicesInterface;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/superapp/servicios")
public class ServicesController {

	@Autowired
	CustomerTyTInterface customerTyTInterfaceLocal;

	@Autowired
	ServicesInterface servicesInterfaceLocal;

	@Autowired
	CustomerInterface customerInterfaceLocal;

	@Autowired
	RequestLogInterface requestLogInterfaceLocal;

	public ServicesController(CustomerTyTInterface customerTyTInterfaceLocal,
			ServicesInterface servicesInterfaceLocal) {
		this.customerTyTInterfaceLocal = customerTyTInterfaceLocal;
		this.servicesInterfaceLocal = servicesInterfaceLocal;
	}

	@GetMapping("/frecuentes/buc/{buc}/personalIdentifier/{personalIdentifier}")
	public ResponseEntity<Object> getCustomerServices(@PathVariable("buc") Integer buc,
			@PathVariable("personalIdentifier") String personalIdentifier) { // @RequestHeader("Authorization") String authorization, @RequestHeader("x-geolocation") String geolocation,
		try {
			//RequestLogEntity _log = new RequestLogEntity(
			//		"/superapp/servicios/frecuentes/buc/{buc}/personalIdentifier/{personalIdentifier}", "GET",
			//		authorization, geolocation, null);
			//requestLogInterfaceLocal.save(_log);

			JSONObject _return = new JSONObject();

			Optional<CustomerEntity> _customer = customerInterfaceLocal.findById((long) 1);

			// JSONObject _service = new JSONObject();
			List<Object> _services = new ArrayList<Object>();

			_customer.get().getCustomerServices().forEach(service -> {
				JSONObject _service = new JSONObject();

				_service.put("idService", service.getService().getKey());
				_service.put("shortName", service.getService().getShortName());
				_service.put("abbreviatedName", service.getService().getAbbreviatedName());
				_service.put("service", service.getService().getName());
				_service.put("agreement", service.getService().getAgreement());
				_service.put("servicecategory", service.getService().getServiceCategory());
				_service.put("mobilReferer", service.getService().getMobilReferer());
				_service.put("color", service.getService().getColor());
				_services.add(_service);
			});

			_return.put("data", _services);
			_return.put("notifications", JSONObject.NULL);

			return new ResponseEntity<>(_return.toString(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/buc/{buc}/personalIdentifier/{personalIdentifier}")
	public ResponseEntity<Object> getAllServices(@PathVariable("buc") Integer buc,
			@PathVariable("personalIdentifier") String personalIdentifier) { // @RequestHeader("Authorization") String authorization, @RequestHeader("x-geolocation") String geolocation, 
		try {
			//RequestLogEntity _log = new RequestLogEntity(
			//		"/superapp/servicios/buc/{buc}/personalIdentifier/{personalIdentifier}", "GET", authorization,
			//		geolocation, null);
			//requestLogInterfaceLocal.save(_log);

			List<Object> _services = new ArrayList<Object>();

			List<ServiceEntity> _servicesEntity = (List<ServiceEntity>) servicesInterfaceLocal.findAll();

			_servicesEntity.forEach(service -> {
				JSONObject _service = new JSONObject();

				_service.put("idService", service.getKey());
				_service.put("name", service.getName());
				_service.put("shortName", service.getShortName());
				_service.put("abbreviatedName", service.getAbbreviatedName());
				_service.put("serviceCategory", service.getServiceCategory());
				_service.put("category", service.getCategory().getName());
				_service.put("mobilReferer", service.getMobilReferer());
				_service.put("agreement", service.getAgreement());
				_service.put("color", service.getColor());
				_services.add(_service);
			});

			return new ResponseEntity<>(_services.toString(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
