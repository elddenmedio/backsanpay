package me.elddenmedio.SanPay1.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import me.elddenmedio.SanPay1.entities.CustomerServicesEntity;

@Repository
//public interface CustomerServicesInterface extends CrudRepository<CustomerServicesEntity, Long> {
public interface CustomerServicesInterface extends JpaRepository<CustomerServicesEntity, Long> {

}
