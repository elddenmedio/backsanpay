package me.elddenmedio.SanPay1.models;

public class PaymentOperationModel {
	private String key;
	private String description;
	private String type;
	private String registration_date;
	private String tracking_key;
	private PaymentOperationAmountModel operation_amount;
	private PaymentPayer payer;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getRegistration_date() {
		return registration_date;
	}

	public void setRegistration_date(String registration_date) {
		this.registration_date = registration_date;
	}

	public String getTracking_key() {
		return tracking_key;
	}

	public void setTracking_key(String tracking_key) {
		this.tracking_key = tracking_key;
	}

	public PaymentOperationAmountModel getOperation_amount() {
		return operation_amount;
	}

	public void setOperation_amount(PaymentOperationAmountModel operation_amount) {
		this.operation_amount = operation_amount;
	}

	public PaymentPayer getPayer() {
		return payer;
	}

	public void setPayer(PaymentPayer payer) {
		this.payer = payer;
	}

	@Override
	public String toString() {
		return "PaymentOperationModel [key=" + key + ", description=" + description + ", type=" + type
				+ ", registration_date=" + registration_date + ", tracking_key=" + tracking_key + ", operation_amount="
				+ operation_amount + ", payer=" + payer + "]";
	}
}
