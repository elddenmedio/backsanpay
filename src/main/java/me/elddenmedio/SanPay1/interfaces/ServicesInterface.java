package me.elddenmedio.SanPay1.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import me.elddenmedio.SanPay1.entities.ServiceEntity;

@Repository
//public interface ServicesInterface extends CrudRepository<ServiceEntity, Long> {
public interface ServicesInterface extends JpaRepository<ServiceEntity, Long> {

//	@Query("SELECT s FROM service s WHERE s.service = ?1")
//	ServicesInterface findByKey(Integer key);
}
