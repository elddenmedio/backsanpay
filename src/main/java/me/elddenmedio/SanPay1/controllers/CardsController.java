package me.elddenmedio.SanPay1.controllers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import me.elddenmedio.SanPay1.entities.BancsEntity;
import me.elddenmedio.SanPay1.entities.CardsEntity;
import me.elddenmedio.SanPay1.entities.CustomerEntity;
import me.elddenmedio.SanPay1.entities.RequestLogEntity;
import me.elddenmedio.SanPay1.interfaces.BancsInterface;
import me.elddenmedio.SanPay1.interfaces.CardsInterface;
import me.elddenmedio.SanPay1.interfaces.CustomerInterface;
import me.elddenmedio.SanPay1.interfaces.RequestLogInterface;
import me.elddenmedio.SanPay1.models.CardModel;
import me.elddenmedio.SanPay1.utils.RandomStringUtils;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/superapp/tarjetas")
public class CardsController {

	@Autowired
	CardsInterface cardsInterfaceLocal;

	@Autowired
	CustomerInterface customerInterfaceLocal;

	@Autowired
	BancsInterface bancsInterfaceLocal;

	@Autowired
	RequestLogInterface requestLogInterfaceLocal;

	private String _error = null;

	public CardsController(CardsInterface cardsInterfaceLocal) {
		this.cardsInterfaceLocal = cardsInterfaceLocal;
	}

	@GetMapping("/buc/{buc}/personalIdentifier/{personalIdentifier}")
	public ResponseEntity<Object> getCustomerCards(@PathVariable("buc") Integer buc,
			@PathVariable("personalIdentifier") String personalIdentifier) { // @RequestHeader("Authorization") String authorization, @RequestHeader("x-geolocation") String geolocation, 
		try {
			//RequestLogEntity _log = new RequestLogEntity(
			//		"/superapp/tarjetas/buc/{buc}/personalIdentifier/{personalIdentifier}", "GET", authorization,
			//		geolocation, null);
			//requestLogInterfaceLocal.save(_log);

			JSONObject _return = new JSONObject();

			Optional<CustomerEntity> _customer = customerInterfaceLocal.findById((long) 1);

			List<Object> _cards = new ArrayList<Object>();

			_customer.get().getCustomerCards().forEach(card -> {
				JSONObject _card = new JSONObject();

				_card.put("banco", card.getBanc().getName());
				_card.put("numeroTarjeta", card.getCardNumber());
				_card.put("aliasTarjeta", card.getCardAlias());
				_card.put("titularTarjeta", card.getCardName());
				_card.put("vencimiento", card.getExpirationDate());
				_card.put("cvv", card.getCvv());
				_card.put("bank_institution_key", card.getBanc().getBank_institution_key());
				_cards.add(_card);
			});

			_return.put("data", _cards);
			_return.put("notifications", JSONObject.NULL);

			return new ResponseEntity<>(_return.toString(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("/agregar")
	public ResponseEntity<Object> postCustomerCard(@RequestHeader("Authorization") String authorization,
			@RequestHeader("x-geolocation") String geolocation, @RequestBody CardModel card) {
		try {
			RequestLogEntity _log = new RequestLogEntity("/superapp/tarjetas/agregar", "POST", authorization,
					geolocation, card.toString());
			requestLogInterfaceLocal.save(_log);

			JSONObject _return = new JSONObject();
			Long randomNum = (long) (10000 + (int) (Math.random() * 9999999));
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

			if (card.getNumeroTarjeta().equals("4111111111111111")) {
				JSONObject _data = new JSONObject();
				RandomStringUtils _randomAlphaNum = new RandomStringUtils(16);

				_data.put("level", "WARNING");
				_data.put("code", "7397b7a111"); // _randomAlphaNum.toString().split("@")[1].concat("11"));
				_data.put("message", "TEMPLATE__Error__TryLate");
				_data.put("timestamp", dateFormat.format(today()));

				_return.put("data", JSONObject.NULL);
				_return.put("notifications", _data);

				return new ResponseEntity<>(_return.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
			} else {
				Optional<CustomerEntity> _customerCards = customerInterfaceLocal.findById((long) 1);
				_customerCards.get().getCustomerCards().forEach(_card -> {
					this._error = null;
					if (_card.getExpirationDate().equals(card.getVencimiento())
							&& _card.getCardNumber().equals(card.getNumeroTarjeta())) {
						this._error = "error";
					}
				});

				if (card.getError() == null) {
					if (_error == null) {
						CustomerEntity _customer = customerInterfaceLocal.getById(1L);
						BancsEntity _banc = bancsInterfaceLocal.getById(2L);

						CardsEntity _cards = new CardsEntity(card.getNumeroTarjeta().toString(), card.getAliasTarjeta(),
								card.getTitularTarjeta(), card.getVencimiento().toString(),
								Integer.parseInt(card.getCvv()), _customer, _banc, null);
						cardsInterfaceLocal.save(_cards);

						RandomStringUtils _randomAlphaNum = new RandomStringUtils(8);

						JSONObject _data = new JSONObject();
						_data.put("authorization_code", randomNum);
						_data.put("display_card_number",
								card.getNumeroTarjeta().substring(card.getNumeroTarjeta().length() - 4));
						_data.put("banco", _banc.getName());
						_data.put("numeroTarjeta", card.getNumeroTarjeta()); // _customer2.get().getCustomerCards().get(_size
																				// -1).getCardNumber());
						_data.put("fechaOperacion", dateFormat.format(today()));
						_data.put("referencia", _randomAlphaNum.toString().split("@")[1]);

						_return.put("data", _data);
						_return.put("notifications", JSONObject.NULL);
					} else {
						JSONObject _data = new JSONObject();
						RandomStringUtils _randomAlphaNum = new RandomStringUtils(16);

						_data.put("level", "WARNING");
						_data.put("code", "7397b7a100"); // _randomAlphaNum.toString().split("@")[1].concat("11"));
						_data.put("message", "La tarjeta que intenta agregar, ya existe actualmente.");
						_data.put("timestamp", dateFormat.format(today()));

						_return.put("data", JSONObject.NULL);
						_return.put("notifications", _data);

						return new ResponseEntity<>(_return.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
					}
				} else {
					return new ResponseEntity<>(HttpStatus.NO_CONTENT);
				}
			}

			return new ResponseEntity<>(_return.toString(), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	private Date yesterday() {
		final Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		return cal.getTime();
	}

	private Date today() {
		final Calendar cal = Calendar.getInstance();
		return cal.getTime();
	}
}
