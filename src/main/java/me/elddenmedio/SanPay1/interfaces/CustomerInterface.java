package me.elddenmedio.SanPay1.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import me.elddenmedio.SanPay1.entities.CustomerEntity;

@Repository
//public interface CustomerInterface extends CrudRepository<CustomerEntity, Long> {
public interface CustomerInterface extends JpaRepository<CustomerEntity, Long> {
	CustomerEntity findByBuc(Integer buc);
}
