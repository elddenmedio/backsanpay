package me.elddenmedio.SanPay1.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.sun.istack.NotNull;

@Entity
@Table(name = "cat_service")
public class CategoryServiceEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "name")
	@NotNull
	private String name;
	
	@OneToMany(mappedBy = "category", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	private List<ServiceEntity> categoryServices = new ArrayList<>();

	public CategoryServiceEntity() {
	}

	public CategoryServiceEntity(String name, List<ServiceEntity> categoryServices) {
		this.name = name;
		this.categoryServices = categoryServices;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ServiceEntity> getCategoryServices() {
		return categoryServices;
	}

	public void setCategoryServices(List<ServiceEntity> categoryServices) {
		this.categoryServices = categoryServices;
	}

	@Override
	public String toString() {
		return "CategoryServiceEntity [id=" + id + ", name=" + name + ", categoryServices=" + categoryServices + "]";
	}
}
