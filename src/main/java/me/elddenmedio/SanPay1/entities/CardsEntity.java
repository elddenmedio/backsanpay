package me.elddenmedio.SanPay1.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.sun.istack.NotNull;

@Entity
@Table(name = "cards")
public class CardsEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "carNumber")
	@NotNull
	private String cardNumber;

	@Column(name = "cardAlias")
	@NotNull
	private String cardAlias;

	@Column(name = "cardName")
	@NotNull
	private String cardName;

	@Column(name = "expirationDate")
	@NotNull
	private String expirationDate;

	@Column(name = "cvc")
	@NotNull
	private Integer cvv;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "customer_id", nullable = false)
	private CustomerEntity customer;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "banc_id", nullable = false)
	private BancsEntity banc;

	@OneToMany(mappedBy = "card", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	private List<PaymentEntity> payments = new ArrayList<>();

	public CardsEntity() {
	}

	public CardsEntity(String cardNumber, String cardAlias, String cardName, String expirationDate, Integer cvv,
			CustomerEntity customer, BancsEntity banc, List<PaymentEntity> payments) {
		this.cardNumber = cardNumber;
		this.cardAlias = cardAlias;
		this.cardName = cardName;
		this.expirationDate = expirationDate;
		this.cvv = cvv;
		this.customer = customer;
		this.banc = banc;
		this.payments = payments;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getCardAlias() {
		return cardAlias;
	}

	public void setCardAlias(String cardAlias) {
		this.cardAlias = cardAlias;
	}

	public String getCardName() {
		return cardName;
	}

	public void setCardName(String cardName) {
		this.cardName = cardName;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	public Integer getCvv() {
		return cvv;
	}

	public void setCvv(Integer cvv) {
		this.cvv = cvv;
	}

	public CustomerEntity getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerEntity customer) {
		this.customer = customer;
	}

	public BancsEntity getBanc() {
		return banc;
	}

	public void setBanc(BancsEntity banc) {
		this.banc = banc;
	}

	public List<PaymentEntity> getPayments() {
		return payments;
	}

	public void setPayments(List<PaymentEntity> payments) {
		this.payments = payments;
	}

	@Override
	public String toString() {
		return "CardsEntity [id=" + id + ", cardNumber=" + cardNumber + ", cardAlias=" + cardAlias + ", cardName="
				+ cardName + ", expirationDate=" + expirationDate + ", cvv=" + cvv + ", customer=" + customer
				+ ", banc=" + banc + ", payments=" + payments + "]";
	}
}
