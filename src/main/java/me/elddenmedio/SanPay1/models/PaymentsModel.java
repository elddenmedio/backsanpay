package me.elddenmedio.SanPay1.models;

public class PaymentsModel {

	private String payee_number_alias;
	private PaymentOperationModel operation;

	public String getPayee_number_alias() {
		return payee_number_alias;
	}

	public void setPayee_number_alias(String payee_number_alias) {
		this.payee_number_alias = payee_number_alias;
	}

	public PaymentOperationModel getOperation() {
		return operation;
	}

	public void setOperation(PaymentOperationModel operation) {
		this.operation = operation;
	}

	@Override
	public String toString() {
		return "PaymentsModel [payee_number_alias=" + payee_number_alias + ", operation=" + operation + "]";
	}
}
