package me.elddenmedio.SanPay1.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.sun.istack.NotNull;
import com.sun.istack.Nullable;

@Entity
@Table(name = "customers")
@NamedQueries({ @NamedQuery(name = CustomerEntity.findMe, query = "SELECT c FROM CustomerEntity c") })

public class CustomerEntity implements Serializable {

	public static final String findMe = "CustomerEntity.findMe";

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "name")
	@NotNull
	private String name;

	@Column(name = "second_name")
	@NotNull
	private String second_name;

	@Column(name = "last_name")
	@Nullable
	private String last_name;

	@Column(name = "buc")
	@NotNull
	private Integer buc;

	@Column(name = "personalIdentifier")
	@NotNull
	private String personalIdentifier;

	@Column(name = "key")
	@NotNull
	private String key;

	@Column(name = "type")
	@NotNull
	private String type;

	@Column(name = "status")
	@NotNull
	private Boolean status;

	@Column(name = "bank_segment_key")
	@NotNull
	private String bank_segment_key;

	@Column(name = "bank_segment_name")
	@NotNull
	private String bank_segment_name;

	@Column(name = "contact_method")
	@NotNull
	private String contact_method;

	@Column(name = "contact_info_type")
	@NotNull
	private String contact_info_type;

	@Column(name = "contact_info_phone_number")
	@NotNull
	private String contact_info_phone_number;

	@Column(name = "contact_info_mobile_number")
	@NotNull
	private String contact_info_mobile_number;

	@Column(name = "contact_info_email")
	@NotNull
	private String contact_info_email;

	@Column(name = "contact_info_phone_status")
	@NotNull
	private String contact_info_phone_status;

	@Column(name = "last_connection")
	@NotNull
	private String last_connection;

	@OneToMany(mappedBy = "customer", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	private List<CardsEntity> customerCards = new ArrayList<>();

	@OneToMany(mappedBy = "customer", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	private List<CustomerServicesEntity> customerServices = new ArrayList<>();

	@OneToMany(mappedBy = "customer", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	private List<PaymentEntity> customerPayments = new ArrayList<>();

	public CustomerEntity() {
	}

	public CustomerEntity(String name, String second_name, String last_name, Integer buc, String personalIdentifier,
			String key, String type, Boolean status, String bank_segment_key, String bank_segment_name,
			String contact_method, String contact_info_type, String contact_info_phone_number,
			String contact_info_mobile_number, String contact_info_email, String contact_info_phone_status,
			String last_connection, List<CardsEntity> customerCards, List<CustomerServicesEntity> customerServices,
			List<PaymentEntity> customerPayments) {
		this.name = name;
		this.second_name = second_name;
		this.last_name = last_name;
		this.buc = buc;
		this.personalIdentifier = personalIdentifier;
		this.key = key;
		this.type = type;
		this.status = status;
		this.bank_segment_key = bank_segment_key;
		this.bank_segment_name = bank_segment_name;
		this.contact_method = contact_method;
		this.contact_info_type = contact_info_type;
		this.contact_info_phone_number = contact_info_phone_number;
		this.contact_info_mobile_number = contact_info_mobile_number;
		this.contact_info_email = contact_info_email;
		this.contact_info_phone_status = contact_info_phone_status;
		this.last_connection = last_connection;
		this.customerCards = customerCards;
		this.customerServices = customerServices;
		this.customerPayments = customerPayments;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSecond_name() {
		return second_name;
	}

	public void setSecond_name(String second_name) {
		this.second_name = second_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public Integer getBuc() {
		return buc;
	}

	public void setBuc(Integer buc) {
		this.buc = buc;
	}

	public String getPersonalIdentifier() {
		return personalIdentifier;
	}

	public void setPersonalIdentifier(String personalIdentifier) {
		this.personalIdentifier = personalIdentifier;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public String getBank_segment_key() {
		return bank_segment_key;
	}

	public void setBank_segment_key(String bank_segment_key) {
		this.bank_segment_key = bank_segment_key;
	}

	public String getBank_segment_name() {
		return bank_segment_name;
	}

	public void setBank_segment_name(String bank_segment_name) {
		this.bank_segment_name = bank_segment_name;
	}

	public String getContact_method() {
		return contact_method;
	}

	public void setContact_method(String contact_method) {
		this.contact_method = contact_method;
	}

	public String getContact_info_type() {
		return contact_info_type;
	}

	public void setContact_info_type(String contact_info_type) {
		this.contact_info_type = contact_info_type;
	}

	public String getContact_info_phone_number() {
		return contact_info_phone_number;
	}

	public void setContact_info_phone_number(String contact_info_phone_number) {
		this.contact_info_phone_number = contact_info_phone_number;
	}

	public String getContact_info_mobile_number() {
		return contact_info_mobile_number;
	}

	public void setContact_info_mobile_number(String contact_info_mobile_number) {
		this.contact_info_mobile_number = contact_info_mobile_number;
	}

	public String getContact_info_email() {
		return contact_info_email;
	}

	public void setContact_info_email(String contact_info_email) {
		this.contact_info_email = contact_info_email;
	}

	public String getContact_info_phone_status() {
		return contact_info_phone_status;
	}

	public void setContact_info_phone_status(String contact_info_phone_status) {
		this.contact_info_phone_status = contact_info_phone_status;
	}

	public String getLast_connection() {
		return last_connection;
	}

	public void setLast_connection(String last_connection) {
		this.last_connection = last_connection;
	}

	public List<CardsEntity> getCustomerCards() {
		return customerCards;
	}

	public void setCustomerCards(List<CardsEntity> customerCards) {
		this.customerCards = customerCards;
	}

	public List<CustomerServicesEntity> getCustomerServices() {
		return customerServices;
	}

	public void setCustomerServices(List<CustomerServicesEntity> customerServices) {
		this.customerServices = customerServices;
	}

	public List<PaymentEntity> getCustomerPayments() {
		return customerPayments;
	}

	public void setCustomerPayments(List<PaymentEntity> customerPayments) {
		this.customerPayments = customerPayments;
	}

	@Override
	public String toString() {
		return "CustomerEntity [id=" + id + ", name=" + name + ", second_name=" + second_name + ", last_name="
				+ last_name + ", buc=" + buc + ", personalIdentifier=" + personalIdentifier + ", key=" + key + ", type="
				+ type + ", status=" + status + ", bank_segment_key=" + bank_segment_key + ", bank_segment_name="
				+ bank_segment_name + ", contact_method=" + contact_method + ", contact_info_type=" + contact_info_type
				+ ", contact_info_phone_number=" + contact_info_phone_number + ", contact_info_mobile_number="
				+ contact_info_mobile_number + ", contact_info_email=" + contact_info_email
				+ ", contact_info_phone_status=" + contact_info_phone_status + ", last_connection=" + last_connection
				+ ", customerCards=" + customerCards + ", customerServices=" + customerServices + ", customerPayments="
				+ customerPayments + "]";
	}
}
